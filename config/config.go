/*
 * Copyright (C) 2021 İrem Kuyucu <siren@kernal.eu>
 *
 * This file is part of kDonate.
 *
 * kDonate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kDonate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kDonate.  If not, see <https://www.gnu.org/licenses/>.
 */

package config

import "github.com/namsral/flag"

type data struct {
	BindAddr string
	MoneropayAddr string
	CallbackAddr string
	MatrixToken string
	MatrixEndpoint string
}

var Values data

func Init() {
	flag.StringVar(&Values.BindAddr, "bind", "0.0.0.0:8080", "Bind ip:port")
	flag.StringVar(&Values.MoneropayAddr, "moneropay-addr",
	    "http://moneropayd:5000/receive", "MoneroPay address")
	flag.StringVar(&Values.CallbackAddr, "callback-addr",
	    "http://donation:8080/callback", "Callback address")
	flag.StringVar(&Values.MatrixToken, "matrix-token", "",
	    "Matrix token for authentication")
	flag.StringVar(&Values.MatrixEndpoint, "matrix-endpoint", "",
	    "Matrix endpoint for sending room message")
	flag.Parse()
}
