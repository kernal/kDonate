/*
 * Copyright (C) 2021 İrem Kuyucu <siren@kernal.eu>
 *
 * This file is part of kDonate.
 *
 * kDonate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kDonate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kDonate.  If not, see <https://www.gnu.org/licenses/>.
 */

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"gitlab.com/moneropay/moneropay/v2/pkg/model"
	"gitlab.com/moneropay/go-monero/walletrpc"

	"gitlab.com/kernal/donation-page/config"
	"gitlab.com/kernal/donation-page/matrix"
)

var index *template.Template
var addr *template.Template
var internalErr *template.Template

func main() {
	config.Init()

	// Route static/ for fonts and css
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	// Pre-render html
	var err error
	index, err = template.ParseFiles("html/index.html")
	if err != nil {
		log.Fatal(err)
	}
	addr, err = template.ParseFiles("html/subaddress.html")
	if err != nil {
		log.Fatal(err)
	}
	internalErr, err = template.ParseFiles("html/error.html")
	if err != nil {
		log.Fatal(err)
	}

	http.HandleFunc("/", sendIndex)
	http.HandleFunc("/callback", getCallback)
	http.HandleFunc("/subaddress", getSubaddress)

	log.Fatal(http.ListenAndServe(config.Values.BindAddr, nil))
}

func sendIndex(w http.ResponseWriter, r *http.Request) {
	index.Execute(w, nil)
}

func getSubaddress(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	jr := model.ReceivePostRequest{
		Amount: 0,
		Description: r.FormValue("description"),
		CallbackUrl: config.Values.CallbackAddr,
	}
	b := new(bytes.Buffer)
	if err := json.NewEncoder(b).Encode(jr); err != nil {
		log.Println(err)
		internalErr.Execute(w, nil)
		return
	}
	req, err := http.NewRequest("POST", config.Values.MoneropayAddr, b)
	if err != nil {
		log.Println(err)
		internalErr.Execute(w, nil)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	cl := &http.Client{Timeout: 15 * time.Second}
	resp, err := cl.Do(req)
	if err != nil {
		log.Println(err)
		internalErr.Execute(w, nil)
		return
	}
	var mr model.ReceivePostResponse
	if err := json.NewDecoder(resp.Body).Decode(&mr); err != nil {
		log.Println(err)
		internalErr.Execute(w, nil)
		return
	}
	items := struct {
		Subaddress string
	}{
		Subaddress: mr.Address,
	}
	err = addr.Execute(w, items)
	if err != nil {
		log.Fatal(err)
	}
}

func getCallback(w http.ResponseWriter, r *http.Request) {
	var data model.CallbackResponse
	if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
		log.Println(err)
		internalErr.Execute(w, nil)
		return
	}
	if data.Transaction.Locked {
		return
	}
	var d string
	if len(data.Description) > 0 {
		d = fmt.Sprintf("Received %s XMR with the description: %s",
		    walletrpc.XMRToDecimal(data.Transaction.Amount), data.Description)
	} else {
		d = fmt.Sprintf("Received %s XMR", walletrpc.XMRToDecimal(data.Transaction.Amount))
	}
	matrix.SendMessage(d)
}
