/*
 * Copyright (C) 2021 İrem Kuyucu <siren@kernal.eu>
 *
 * This file is part of kDonate.
 *
 * kDonate is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * kDonate is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with kDonate.  If not, see <https://www.gnu.org/licenses/>.
 */

 package matrix

import (
	"bytes"
	"encoding/json"
	"log"
	"crypto/rand"
	"net/http"

	"gitlab.com/kernal/donation-page/config"
)

type Message struct {
	MsgType string `json:"msgtype"`
	Body string `json:"body"`
}

func SendMessage(msg string) {
	m := Message{
		MsgType: "m.text",
		Body: msg,
	}

	j, _ := json.Marshal(m)
	p, _ := rand.Prime(rand.Reader, 64)
	req, _ := http.NewRequest(http.MethodPut,
	    config.Values.MatrixEndpoint + p.String(), bytes.NewBuffer(j))
	req.Header.Set("Authorization", "Bearer " + config.Values.MatrixToken)

	client := &http.Client{}
	_, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
}
