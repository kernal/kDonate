module gitlab.com/kernal/donation-page

go 1.16

require (
	github.com/namsral/flag v1.7.4-pre
	gitlab.com/moneropay/go-monero v1.1.0
	gitlab.com/moneropay/moneropay/v2 v2.3.0
)
